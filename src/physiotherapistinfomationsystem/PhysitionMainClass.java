/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package physiotherapistinfomationsystem;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author iron_pc
 */
public class PhysitionMainClass {
    
    public static void main(String args[]) throws FileNotFoundException, IOException{
        Scanner scan = new Scanner(System.in);
        Appointment appointment = new Appointment();
        int phy_id = 0;
        
        Physician physician = new Physician();
        physician.physicianDetails();
        
        System.out.println("Which physician would you like to consult? Please enter physicianID");
        phy_id = Integer.parseInt(scan.nextLine());
       
        switch(phy_id){
            case 101: appointment.printReciept(phy_id);
                      break;
            case 102: appointment.printReciept(phy_id);
                      break;
            case 103: appointment.printReciept(phy_id);
                      break;
            default: System.out.println("No Physiciant found.");
        }
        System.out.println("Do you want to see history of patient? (yes or no)");
        String is_ans = scan.nextLine();
        if(is_ans.equalsIgnoreCase("yes")){
            System.out.println("******** Record of Patient **********");
            Patient patient = new Patient();
            patient.displayHistoryOfPatient();
        }else{
            System.exit(0);
        }
    }
}
