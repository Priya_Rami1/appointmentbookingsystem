/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package physiotherapistinfomationsystem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import static java.lang.System.in;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author iron_pc
 */
public class Physician {

    
    private static int phy_ID;
    private static String phy_Name;
    private static String phy_exp_Area;
    private static String phy_Available_DateTime;
    private static String phy_RoomName;
    private static String phy_Treatment;
    
    public Physician(){}
    
    public Physician(int phy_Id, String phy_Name, String phy_exp_Area,String phy_Avalilable_DateTime, String roomName, String phy_Treatment){
        this.phy_ID   = phy_Id;
        this.phy_Name = phy_Name;
        this.phy_exp_Area = phy_exp_Area;
        this.phy_Available_DateTime = phy_Avalilable_DateTime;
        this.phy_RoomName = roomName;
        this.phy_Treatment = phy_Treatment;
    }
    
    public static void setPhy_ID(int phy_ID) {
        Physician.phy_ID = phy_ID;
    }

    public static void setPhy_Name(String phy_Name) {
        Physician.phy_Name = phy_Name;
    }

    public static void setPhy_exp_Area(String phy_exp_Area) {
        Physician.phy_exp_Area = phy_exp_Area;
    }

    public static void setPhy_Available_DateTime(String phy_Available_DateTime) {
        Physician.phy_Available_DateTime = phy_Available_DateTime;
    }

    public static void setPhy_RoomName(String phy_RoomName) {
        Physician.phy_RoomName = phy_RoomName;
    }

    public static void setPhy_Treatment(String phy_Treatment) {
        Physician.phy_Treatment = phy_Treatment;
    }

    public static int getPhy_ID() {
        return phy_ID;
    }

    public static String getPhy_Name() {
        return phy_Name;
    }

    public static String getPhy_exp_Area() {
        return phy_exp_Area;
    }

    public static String getPhy_Available_DateTime() {
        return phy_Available_DateTime;
    }

    public static String getPhy_RoomName() {
        return phy_RoomName;
    }

    public static String getPhy_Treatment() {
        return phy_Treatment;
    }

    @Override
    public String toString() {
        System.out.println("*****************************************************");
        System.out.println("Physician Details");
        System.out.println("*****************************************************");
        return "PhysicienID: " + getPhy_ID() + " Physicien Name: " + getPhy_Name() + " Room Name: " + getPhy_RoomName();
    }
    
    //Display List of avalilable Physician 
    public void physicianDetails() throws FileNotFoundException, IOException{
         File file = new File("sampleFile/ListOfPhysician_2.csv");
         List<String> lines = Files.readAllLines(file.toPath(),StandardCharsets.UTF_8);
    System.out.println("******* List of Physician *********");
    System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
    System.out.printf("%5s | %15s     | %15s| %10s      | %5s      | %5s", "PhysicianID", "Physician Name", "Physician Experties","physician Avaliable DateTime", "Physician Room", "PhysicinTreatment");
    System.out.println();
    System.out.println("----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
         for (String line : lines) {
            String[] array = line.split(",");
            System.out.format("%11s | %15s | %5s    | %10s | %5s | %5s",
                array[0], array[1], array[2], array[3],array[4],array[5]);
               System.out.println();
        }
     System.out.println("-----------------------------------------------------------------------------");
    }
}
    