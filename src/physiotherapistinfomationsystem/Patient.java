/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package physiotherapistinfomationsystem;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author iron_pc
 */
public class Patient {
    
   private static int p_ID;
   private static String p_Name;
   private static String p_Address;
   private static int p_Age;
   private static String p_Gender;
   private static Long p_Telnumber;

    public Patient(int p_ID, String p_Name,String p_Address,int p_Age,String p_Gender,long p_Telnumber) {
       this.p_ID        = p_ID;
       this.p_Name      = p_Name;
       this.p_Address   = p_Address;
       this.p_Age       = p_Age;
       this.p_Gender    = p_Gender;
       this.p_Telnumber = p_Telnumber;
    }
    
    public Patient(){}

    public static void setP_ID(int p_ID) {
        Patient.p_ID = p_ID;
    }

    public static void setP_Name(String p_Name) {
        Patient.p_Name = p_Name;
    }

    public static void setP_Address(String p_Address) {
        Patient.p_Address = p_Address;
    }

    public static void setP_Age(int p_Age) {
        Patient.p_Age = p_Age;
    }

    public static void setP_gender(String p_Gender) {
        Patient.p_Gender = p_Gender;
    }

    public static void setP_Telnumber(long p_Telnumber) {
        Patient.p_Telnumber = p_Telnumber;
    }
    
    public static int getP_ID() {
        return p_ID;
    }

    public static String getP_Name() {
        return p_Name;
    }

    public static String getP_Address() {
        return p_Address;
    }

    public static int getP_Age() {
        return p_Age;
    }

    public static String getP_gender() {
        return p_Gender;
    }

    public static long getP_Telnumber() {
        return p_Telnumber;
    }

    @Override
    public String toString() {
        System.out.println("*****************************************************");
        System.out.println("Patent Details");
        System.out.println("*****************************************************");
        return "PatientID: "+ this.p_ID +" Name: " + this.p_Name + " Age: " + this.p_Age + 
                " Gender: " + this.p_Gender;
        
    }  
    
    public void displayHistoryOfPatient() throws IOException{
       File file = new File("sampleFile/Patients Detail.csv");
        List<String> lines = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
        for (String line : lines) {
            String[] array = line.split(",");
            System.out.format("%10s | %20s | %31s | %10s | %10s | %10s | %10s | %10s | %10s | %10s",
                array[0], array[1], array[2], array[3],array[4],array[5],array[6],array[7],array[8],array[9]);
            System.out.println();
        } 
    }
}






