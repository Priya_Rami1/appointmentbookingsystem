/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package physiotherapistinfomationsystem;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iron_pc
 */
public class Appointment {

    private static int ap_ID;
    private static String ap_time;
    private static String ap_Date;
    private static String ap_checkin;
    private static String ap_Status;

    static Scanner scan = new Scanner(System.in);
    static Patient patient = new Patient();
    static Physician physician = new Physician();
    static Random random = new Random();

    public Appointment(int ap_ID, String ap_Date, String ap_time, String ap_checkin, String ap_Status) {
        Appointment.ap_ID = ap_ID;
        Appointment.ap_time = ap_time;
        Appointment.ap_Date = ap_Date;
        Appointment.ap_checkin = ap_checkin;
        Appointment.ap_Status = ap_Status;
    }

    public Appointment() {
    }

    public static int getAp_ID() {
        return ap_ID;
    }

    public static String getAp_time() {
        return ap_time;
    }

    public static String getAp_Date() {
        return ap_Date;
    }

    public static String getAp_checkin() {
        return ap_checkin;
    }

    public static String getAp_Status() {
        return ap_Status;
    }

    public static void setAp_ID(int ap_ID) {
        Appointment.ap_ID = ap_ID;
    }

    public static void setAp_time(String ap_time) {
        Appointment.ap_time = ap_time;
    }

    public static void setAp_Date(String ap_Date) {
        Appointment.ap_Date = ap_Date;
    }

    public static void setAp_checkin(String ap_checkin) {
        Appointment.ap_checkin = ap_checkin;
    }

    public static void setAp_Status(String ap_Status) {
        Appointment.ap_Status = ap_Status;
    }

    @Override
    public String toString() {
        Date date = new Date();
        try {
            SimpleDateFormat dateformat = new SimpleDateFormat("dd/MM/yyyy");
            date = dateformat.parse(getAp_Date());

        } catch (ParseException ex) {
            Logger.getLogger(Appointment.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("*****************************************************");
        System.out.println("Appointment Details");
        System.out.println("*****************************************************");
        return "AppointmentID: " + getAp_ID() + "\nAppointment Date: " + date + " Appointment Time: " + getAp_time() + 
               " Appointment Status: " + getAp_Status();
    }

    //Patien registration Form 
    public static Patient patientDetailInput() {

        patient.setP_ID(random.nextInt(1000));
        do {
            System.out.println("Enter a patient Name: ");
            patient.setP_Name(scan.nextLine());

            System.out.println("Enter a patient Address: ");
            patient.setP_Address(scan.nextLine());

            if (patient.getP_Name().isEmpty() || patient.getP_Address().isEmpty()) {
                System.out.println("Please enter your pateint Name or Address is missing.");
            }

        } while (patient.getP_Name().isEmpty() || patient.getP_Address().isEmpty());

        System.out.println("Enter a patient TelephoneNumber: ");
        patient.setP_Telnumber(Long.parseLong(scan.nextLine()));

        if (patient.getP_Telnumber() < 12) {
            System.out.println("Please enter a correct Number.");
        }

        System.out.println("Enter a patient Age: ");
        patient.setP_Age(Integer.parseInt(scan.nextLine()));

        System.out.println("Select gender of patient: (Male, Female and other)");
        String gender = scan.nextLine();

        if (gender.equalsIgnoreCase("male") || gender.equalsIgnoreCase("Female") || gender.equalsIgnoreCase("other")) {
            patient.setP_gender(gender);
        } else {
            System.out.println("You have a wrong selection.");
        }
        return patient;
    }

    //If any of physicint select set a value to Physicien
    public static Physician selectedPhysician(int phy_id) throws IOException {
        File file = new File("sampleFile/ListOfPhysician_2.csv");
        List<String> lines = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
        for (String line : lines) {
            String[] array = line.split(",");
            boolean isPhID = Integer.parseInt(array[0]) == phy_id;
            if (isPhID) {
                Physician.setPhy_ID(phy_id);
                Physician.setPhy_Name(array[1]);
                Physician.setPhy_exp_Area(array[2]);
                Physician.setPhy_Available_DateTime(array[3]);
                Physician.setPhy_RoomName(array[4]);
                Physician.setPhy_Treatment(array[5]);
            }
        }
        return physician;
    }

    public Appointment bookAppointment() {
        setAp_ID(random.nextInt(1000));
        System.out.println("Enter time to consult between avilable time slot (HH:MM AM/PM): ");
        Appointment.setAp_time(scan.nextLine());

        System.out.println("Enter date to consult (dd/MM/yyyy): ");
        Appointment.setAp_Date(scan.nextLine());

        System.out.println("would you like to confirm appointment? (yes or no)");
        Appointment.setAp_checkin(scan.nextLine());
        if (getAp_checkin().equalsIgnoreCase("yes")) {
            setAp_Status("Confirmed");
        } else {
            setAp_Status("Canceled");
        }
        return this;
    }

    public void printReciept(int ph_ID) throws IOException {
        Patient patientDetail = patientDetailInput();
        Appointment appointDetail = bookAppointment();
        
        if (getAp_Status().equalsIgnoreCase("Canceled")) {
            System.out.println("Sorry you appointment is canceled.");
        } else if(getAp_Status().equalsIgnoreCase("Confirmed")){
            setAp_Status("Attended");
            System.out.println(patientDetail);
            System.out.println(appointDetail);
            System.out.println(selectedPhysician(ph_ID));
            System.out.println("*****************************************************");
        }else{
            System.out.println("No Appointments");
        }

    }

}
